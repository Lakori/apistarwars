'use strict';

const api = 'https://swapi.co/api/starships/';

function catchApi(url) {
    fetch(url).then(function(response){
        response.json().then(function (result){
            getShip(result);
            
        });
    }).catch(function(e){
        console.log(e);
    })
}

catchApi(api);

function getShip(response){
   let container = document.querySelector('#shipCards');

    if(response.next){
        catchApi(response.next);
    }

    response.results.forEach(ship => {
        let card = document.createElement('div');
        card.setAttribute('class', 'ship-card');
       
        let name = document.createElement('h4');
        name.setAttribute('class', 'ship-title');
        name.textContent = ship.name;

        let model = document.createElement('p');
        model.setAttribute('class', 'ship-model');
        model.textContent = `model: ${ship.model}`;

        let manufacturer = document.createElement('p');
        manufacturer.setAttribute('class', 'ship-model');
        manufacturer.textContent = `manufacturer: ${ship.manufacturer}`;

        card.appendChild(name);
        card.appendChild(model);
        card.appendChild(manufacturer);
        container.appendChild(card);
    })
   
}